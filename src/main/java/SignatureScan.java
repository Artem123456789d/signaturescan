import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class SignatureScan {

    private static Deque<String> directories = new ArrayDeque<>();
    private static List<String> fileCopies = new ArrayList<>();
    private static List<Byte> originSignature = new ArrayList<>();

    //Файл для поиска копий
    private static String FILE;
    //Стартовая директория для поиска
    private static String DIRECTORY;
    //Смещение
    private static int OFFSET;
    //Длина сигнатуры
    private static int SIGNATURE_LENGTH;

    private static void init(String dirPath) {
        //инициализация(Чтение файлов/папок в стартовой директории)
        File directory = new File(dirPath);
        String [] directoryContents = directory.list();

        //Если стартовая директория пуста
        if (directoryContents == null) {
            System.out.println("Стартовая директория пуста");
            return;
        }

        File originFile = new File(FILE);

        //Если исходный файл -- директория
        if (!originFile.isFile()) {
            System.out.println("Исходный файл -- не файл");
            return;
        }

        //Если длина исходного файла меньше, чем смещение -- не обрабатывать
        if (originFile.length() < OFFSET) {
            System.out.println("Длина исходного файла меньше смещения");
            return;
        }

        //Первоначальный обход стартовой директории
        for(String fileName: directoryContents) {
            File temp = new File(String.valueOf(directory), fileName);
            directories.offer(temp.getAbsolutePath());
        }

        try {
            //чтение сигнатуры исходного файла
            FileInputStream inputStream = new FileInputStream(originFile);
            //Если длина файла меньше, чем смещение + длина сигнатуры, то считываем максимально возможное количество
            //байт из файла
            long actualSignatureLength = Math.min(SIGNATURE_LENGTH, originFile.length() - OFFSET);
            byte[] b = new byte[inputStream.available()];

            if (actualSignatureLength != SIGNATURE_LENGTH) {
                System.out.println("Для исходного файла будет использована длина сигнатуры " + actualSignatureLength);
            }

            //отбрасываем первые OFFSET бит
            inputStream.skip(OFFSET);
            //Считываем сигнатуру возможной длины
            inputStream.read(b, 0, (int) actualSignatureLength);

            //записываем в специальную коллекцию сигнатуру
            for (int i = 0; i < actualSignatureLength; i++) {
                originSignature.add(b[i]);
            }

        } catch (FileNotFoundException e) {
            System.out.println("Исходный файл не найден");
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static boolean equalToOrigFile (int compSize, byte[] b) {
        //сравнение сигнатуры очередного файла с сигнатурой исходного файла
        for (int i = 0; i < compSize; i++) {
            if (b[i] != originSignature.get(i)) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите путь к файлу: ");
        FILE = in.next();
        System.out.print("Введите путь к директории поиска: ");
        DIRECTORY = in.next();
        System.out.print("Введите смещение: ");
        OFFSET = in.nextInt();
        System.out.print("Введите длину сигнатуры: ");
        SIGNATURE_LENGTH = in.nextInt();

        init(DIRECTORY);

        //Пока не обошли все возможные директории
        while (!directories.isEmpty()) {
            File file = new File(directories.poll());

            //Если очередной путь -- директория
            if (file.isDirectory()) {
                String [] directoryContents = file.list();

                //Записываем в очередь путь к очередной директории
                for(String fileName: directoryContents) {
                    File temp = new File(String.valueOf(file), fileName);
                    directories.offer(temp.getAbsolutePath());
                }
            } else if (file.isFile()) {
                //Если очередной путь -- файл, то сравниваем его сигнатуру и сигнатуру исходного файла
                FileInputStream inputStream = new FileInputStream(file);

                if (file.length() < OFFSET) {
                    System.out.println("Длина файла меньше, чем размер смещения. " + file.getAbsolutePath());
                    continue;
                }
                long actualSignatureLength = Math.min(SIGNATURE_LENGTH, file.length() - OFFSET);
                byte[] b = new byte[inputStream.available()];

                if (actualSignatureLength != SIGNATURE_LENGTH) {
                    System.out.println("Для файла " + file.getAbsolutePath() + " будет использована длина сигнатуры " + actualSignatureLength);
                }

                inputStream.skip(OFFSET);
                inputStream.read(b, 0, (int) actualSignatureLength);

                int compSize = (int) Math.min(originSignature.size(), actualSignatureLength);

                if (equalToOrigFile(compSize, b)) {
                    fileCopies.add(file.getAbsolutePath());
                }
            }
        }
        System.out.println("-------------------------------");
        System.out.println("Найдено копий: " + fileCopies.size());
        for (String path : fileCopies) {
            System.out.println(path);
        }
    }
}
